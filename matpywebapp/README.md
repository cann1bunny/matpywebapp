# Requirements
* Python 3.6.8 with installed dependencies from requirements.txt
* Matlab R2018b installed
* MATLAB Engine API for Python installed to your Python environment

# Installation

1. Open your python environment in project directory
2. Install requirements:
```
pip install -r requirements.txt
```

2. Find the directory Matlab is installed in by from Terminal / Command Prompt:
```
matlab -batch matlabroot
```

3. Change to the directory indicated then:
```
cd extern/engines/python
```

4. Then execute these commands:
* Mac or Linux:
```
python setup.py build --build-base=$(mktemp -d) install
```
* Windows:  
```
python setup.py build --build-base=$env:temp install
```

# Running
In your python environment:
```
python app.py
```
