%% MAIN SECTION
disp('Calculating...');

global X U mz mrr m1 m2 m3 m4 K1 K2 Ifr r0 I23 mb mr k

load t
load x X
load u U

mz=35;
mrr=62.5;
m1=52;
m2=78; 
m3=90;
m4=125;
K1=281.4;
K2=291;
Ifr=82.5;
r0=0.250;
I23=Ifr+((m2+m3+m4)*r0^2);
mb=mz+m1;
mr=mrr+mb+m2;
k=2;
% save k k