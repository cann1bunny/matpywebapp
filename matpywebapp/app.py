# -*- coding: utf-8 -*-
import dash
import dash_bootstrap_components as dbc

# Init Dash app object
app = dash.Dash(__name__, suppress_callback_exceptions=True, external_stylesheets=[dbc.themes.BOOTSTRAP])

# Init server object for production
server = app.server
