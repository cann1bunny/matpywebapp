function [t,y]=RungeKutta(f,tt,pp)
for(i=1:40)
y(1,i)=pp(i);
end
h=0.01;
t=tt(1):h:tt(2);
for k=1:length(t)
    k1=h.*f(t(k),y(k,:))
    k2=h.*f(t(k)+h/2,y(k,:)+k1'./2);
    k3=h.*f(t(k)+h/2,y(k,:)+k2'./2);
    k4=h.*f(t(k)+h,y(k,:)+k3');
    if k~=(length(t))
        y(k+1,:)=y(k,:)+(k1'+2*(k2'+k3')+k4')/6;
    end
end
