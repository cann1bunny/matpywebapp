# Running

1.  Login to GitLab Docker Container Registry:
```
docker login registry.gitlab.com
```

2. Configure 'server_name' string in nginx.conf
```
server {
    listen 80;
    server_name <YOUR DOMAIN NAME OR IP ADRESS>;

    location / {
        include uwsgi_params;
        uwsgi_pass unix:/matpywebapp/matpywebapp.sock;
        uwsgi_read_timeout 300;
    }
}

```

3.  Build docker image:
```
docker build -t matpywebapp:latest .
```

4.  Run container using command:
```
docker run -d -p 80:80 --restart=always --mac-address=xx:xx:xx:xx:xx:xx --name matpywebapp matpywebapp:latest
```