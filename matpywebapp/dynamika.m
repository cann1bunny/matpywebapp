function vystupy = dynamika(theta1_0, theta1_dot_0, theta_dot_dot1, theta2_0, theta2_dot_0, theta_dot_dot2, lambda1, error_dot1, lambda2, error_dot2)
    L1x = 1;
    L2x = 0.5;
    m1 = 0.2500;
    m2 = 0.1250;
    g = 9.80665;
    I1g = 0.0209;
    I2g = 0.0026;
    
    
    
    M11 = I1g + I2g + (L1x^2*m1)/4 + L1x^2*m2 + (L2x^2*m2)/4 + L1x*L2x*m2*cos(theta2_0);
    M12 = (m2*L2x^2)/4 + (L1x*m2*cos(theta2_0)*L2x)/2 + I2g;
    M22 = (m2*L2x^2)/4 + I2g;

    F12 = (L1x*L2x*m2*sin(theta2_0))/2;
%      

    g1 = ((L2x*m2*cos(theta1_0 + theta2_0) + L1x*m1*cos(theta1_0) + 2*L1x*m2*cos(theta1_0)))/2;
    g2 = (L2x*m2*cos(theta1_0 + theta2_0))/2;

    matica_dyn1 = M11 * theta_dot_dot1 + M12 * theta_dot_dot2  - F12 * theta2_dot_0 * theta1_dot_0 - F12 * (theta1_dot_0 + theta2_dot_0) * theta2_dot_0 + g1 * g;
    matica_dyn2 = M12 * theta_dot_dot1 + M22 * theta_dot_dot2 +  F12 * theta1_dot_0 * theta1_dot_0                                                + g2 * g;
    
    vystupy(1,1) = -matica_dyn1 + theta_dot_dot1 - lambda1 * (error_dot1);
    vystupy(2,1) = -matica_dyn2 + theta_dot_dot2 - lambda2 * (error_dot2);
%     output(2,1) = 0.5;

end