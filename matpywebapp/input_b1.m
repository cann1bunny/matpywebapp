%% MAIN SECTION
disp('Calculating...');

syms u1 u2 x(t) r dr ddr fi dfi ddfi z dz ddz K3 u3;
syms p1 p2 p3 p4 g;

global X U mz mrr m1 m2 m3 m4 K1 K2 Ifr r0 I mb mr k;

hustota = 1000;
cas = 0.01;
m1 = 52;
m2 = 78;
m3 = 90;
m4 = 125;
m5 = 141;
mz = 35;
mrr = 62.5;
l = 1;
h = 0.5;
% K1=281.4;
% K2=291;
K1=281.4;
K2=291;
u1 = 0.4;
u2 = 0.6;
Ifr=82.5;
r0=0.250;
I=Ifr+((m2+m3+m4)*r0^2);
mb=mz+m1;
me=mb;
mr=mrr+mb+m2;
k=101;
TT = 2;

% PRE SIMSCAPE
L1x = 1; 
L1y = 0.050; 
L1z = 0.005;
L2x = 0.5; 
L2y = 0.05; 
L2z = 0.005;

disp('[DONE]')

%% SIMULATION SECTION
disp('Simulating...');
sim('model_b1');

% CONVERT COLUMNS TO RAWS IN VARIABLES
tout = tout.';

rs = rS.Data(:,1).';
rs1 = rS1.Data(:,1).';
ras = rAS.Data(:,1).';

fis = fiS.Data(:,1).';
fis1 = fiS1.Data(:,1).';
fias = fiAS.Data(:,1).';

% SAVE VARIABLES TO FILE FOR NEXT USAGE IN PYTHON APP
save('output','tout','rs','rs1','ras','fis','fis1','fias')

disp('[DONE]')
