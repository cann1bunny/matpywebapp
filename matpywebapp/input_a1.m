disp('Calculating...');

hustota = 1000; % Input value from web app
g = 9.80665;
b1_tlm = 0.005;
b2_tlm = 0.005;

L1x; % Input value from web app
L1y = 0.050;
L1z = 0.005;
m1 = hustota * L1x*L1y*L1z;

I1g = m1*(L1x^2 + L1y^2)/12;
I1o = I1g + m1*(L1x/2)^2;

L2x; %  Input value from web app
L2y = 0.05;
L2z = 0.005;
m2 = hustota * L2x*L2y*L2z;

I2g = m2*(L2x^2 + L2y^2)/12;
I2o = I2g + m2*(L2x/2)^2;

theta1_0 = 0.56;
theta1_dot_0 = 0;
theta2_0 = 0.84;
theta2_dot_0 = 0;
alpha_0 = theta1_0 + theta2_0;
alpha_dot_0 = theta1_dot_0 + theta2_dot_0;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% ZADEFINOVANIE PARAMETROV MODELU

syms u1 u2 q_1 q_1p q_2 q_2p
syms m2_s m1_s g_s I1G_s I2G_s
syms n5 n6 n8 n9 L1X_s L2X_s b1_s b2_s
syms t theta1(t) TH1_s TH1D_s TH1DD_s
syms theta2(t) TH2_s TH2D_s TH2DD_s

% DEFINOVANIE POLOHY A JEJ DERIVACII

X1_G = (L1X_s/2)*cos(theta1(t));
Y1_G = (L1X_s/2)*sin(theta1(t));

X1_G_d = diff(X1_G,t);
X1_G_dd = diff(X1_G,t,2);

Y1_G_d = diff(Y1_G,t);
Y1_G_dd = diff(Y1_G, t,2);

alpha = theta1(t) + theta2(t);
X2_G = L1X_s*cos(theta1(t)) + (L2X_s/2)*cos(alpha);
Y2_G = L1X_s*sin(theta1(t)) + (L2X_s/2)*sin(alpha);

X2_G_d = diff(X2_G, t);

X2_G_dd = diff(X2_G,t,2);

Y2_G_d = diff(Y2_G,t);
Y2_G_dd = diff(Y2_G,t,2);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% DEFINOVANIE KARTEZIANSKYCH SURADNIC

Ke_trans = 0.5*m1_s*(X1_G_d^2 + Y1_G_d^2) + 0.5*m2_s*(X2_G_d^2 + Y2_G_d^2);

% NAJDENIE DERIVACII PRE LAGRANGEOVU ROVNICU

theta1_d = diff(theta1,t);
theta2_d = diff(theta2,t);

Ke_rot = 0.5*I1G_s*((theta1_d)^2) + 0.5*I2G_s*((theta1_d + theta2_d)^2);

% VYSLEDNA ROVNICA, KTORA VYJADRUJE KINETICKU ENERGIU

EK = Ke_trans + Ke_rot;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% NAJDENIE ROVNICE POTENCIALNEJ ENERGIE PODOBNE AKO KINETICKEJ

EP = m1_s*g_s*Y1_G + m2_s*g_s*Y2_G;

L_rovnica = EK - EP;
symvar(L_rovnica);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% ZAVEDENIE DOCASNYCH PREMENNYCH

L = subs(L_rovnica,[theta1(t), diff(theta1(t),t),  theta2(t),  diff(theta2(t),t)], ...
                    [  q_1,            q_1p,          q_2,             q_2p]);

dLdq1p = diff(L,q_1p);
dLdq1p = subs(dLdq1p,  [   q_1,            q_1p,          q_2,             q_2p], ...
                        [theta1(t), diff(theta1(t),t),  theta2(t),  diff(theta2(t),t)]);

der_dt_of_dLdq1p = diff(dLdq1p, t);

dLdq1 = diff(L,q_1);
dLdq1 = subs(dLdq1,  [   q_1,            q_1p,          q_2,             q_2p], ...
                        [theta1(t), diff(theta1(t),t),  theta2(t),  diff(theta2(t),t)]);

syms Q1_s
EOM_TH1_LHS = der_dt_of_dLdq1p - dLdq1;
EOM_TH1_LHS = simplify(EOM_TH1_LHS);
EOM_TH1_RHS = Q1_s;
EOM_TH1 = EOM_TH1_LHS == EOM_TH1_RHS;

simplify(EOM_TH1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% POUZITIE DOCASNYCH PREMENNYCH
% DALEJ POSTUP JE ROVNAKY

L = subs(L_rovnica, [theta1(t), diff(theta1(t),t),  theta2(t),  diff(theta2(t),t)], ...
                        [  q_1,            q_1p,          q_2,             q_2p]);

dLdq2p = diff(L, q_2p);
dLdq2p = subs(dLdq2p,  [   q_1,            q_1p,          q_2,             q_2p], ...
                        [theta1(t), diff(theta1(t),t),  theta2(t),  diff(theta2(t),t)]);

der_dt_of_dLdq2p = diff(dLdq2p,t);

dLdq2 = diff(L,q_2);
dLdq2 = subs(dLdq2,  [   q_1,            q_1p,          q_2,             q_2p], ...
                        [theta1(t), diff(theta1(t),t),  theta2(t),  diff(theta2(t),t)]);

syms Q2_s
EOM_TH2_LHS = der_dt_of_dLdq2p - dLdq2;
EOM_TH2_LHS = simplify( EOM_TH2_LHS );
EOM_TH2_RHS = Q2_s;
EOM_TH2     = EOM_TH2_LHS == EOM_TH2_RHS;

simplify(EOM_TH2);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% VYJADRENIE THETA1 A THETA2 Z PREMENNEJ,
% VYMENA NAZU ROVNICE,
% VZAJEMNA VYMENA THETY A JEJ DERIVACIE

staraPremen_1 = [theta1,  diff(theta1,1),   diff(theta1, 2)];
novaPremen_1 = [ TH1_s,          TH1D_s,           TH1DD_s];

staraPremen_2 = [theta2,  diff(theta2,1),   diff(theta2, 2)];
novaPremen_2 = [ TH2_s,          TH2D_s,           TH2DD_s];

EOM_TH1        = subs(EOM_TH1, [staraPremen_1, staraPremen_2], ...
                                [novaPremen_1, novaPremen_2]);

EOM_TH2        = subs(EOM_TH2, [staraPremen_1, staraPremen_2], ...
                                [novaPremen_1, novaPremen_2]);

S = solve([EOM_TH1, EOM_TH2],[TH1DD_s, TH2DD_s]);

DRUHA_TH1 = S.TH1DD_s;
DRUHA_TH2 = S.TH2DD_s;

M = [ I1G_s + I2G_s + (L1X_s^2*m1_s)/4 + L1X_s^2*m2_s + (L2X_s^2*m2_s)/4 + L1X_s*L2X_s*m2_s*cos(TH2_s), (m2_s*L2X_s^2)/4 + (L1X_s*m2_s*cos(TH2_s)*L2X_s)/2 + I2G_s;
        (m2_s*L2X_s^2)/4 + (L1X_s*m2_s*cos(TH2_s)*L2X_s)/2 + I2G_s,                                       (m2_s*L2X_s^2)/4 + I2G_s];

FF = [ -L1X_s*L2X_s*TH2D_s*m2_s*sin(TH2_s),       -(L1X_s*L2X_s*TH2D_s*m2_s*sin(TH2_s))/2;
        (L1X_s*L2X_s*TH1D_s*m2_s*sin(TH2_s))/2,                                           0];

KK = [0 0;
        0 0];

GG = [(g_s*(L2X_s*m2_s*cos(TH1_s + TH2_s) + L1X_s*m1_s*cos(TH1_s) + 2*L1X_s*m2_s*cos(TH1_s)))/2;
                                                        (L2X_s*g_s*m2_s*cos(TH1_s + TH2_s))/2];

QQ1= (2*(8*I2G_s*Q1_s - 8*I2G_s*Q2_s + 2*L2X_s^2*Q1_s*m2_s - 2*L2X_s^2*Q2_s*m2_s - 2*L1X_s*L2X_s^2*g_s*m2_s^2*cos(TH1_s) - 4*L1X_s*L2X_s*Q2_s*m2_s*cos(TH2_s) - 4*I2G_s*L1X_s*g_s*m1_s*cos(TH1_s) - 8*I2G_s*L1X_s*g_s*m2_s*cos(TH1_s) + L1X_s*L2X_s^3*TH1D_s^2*m2_s^2*sin(TH2_s) + L1X_s*L2X_s^3*TH2D_s^2*m2_s^2*sin(TH2_s) + 4*I2G_s*L1X_s*L2X_s*TH1D_s^2*m2_s*sin(TH2_s) + 4*I2G_s*L1X_s*L2X_s*TH2D_s^2*m2_s*sin(TH2_s) - L1X_s*L2X_s^2*g_s*m1_s*m2_s*cos(TH1_s) + 2*L1X_s*L2X_s^3*TH1D_s*TH2D_s*m2_s^2*sin(TH2_s) + 2*L1X_s*L2X_s^2*g_s*m2_s^2*cos(TH1_s + TH2_s)*cos(TH2_s) + 2*L1X_s^2*L2X_s^2*TH1D_s^2*m2_s^2*cos(TH2_s)*sin(TH2_s) + 8*I2G_s*L1X_s*L2X_s*TH1D_s*TH2D_s*m2_s*sin(TH2_s)))/(16*I1G_s*I2G_s + 4*L1X_s^2*L2X_s^2*m2_s^2 + 4*I2G_s*L1X_s^2*m1_s + 4*I1G_s*L2X_s^2*m2_s + 16*I2G_s*L1X_s^2*m2_s + L1X_s^2*L2X_s^2*m1_s*m2_s - 4*L1X_s^2*L2X_s^2*m2_s^2*cos(TH2_s)^2);
QQ2= -(2*(8*I2G_s*Q1_s - 8*I1G_s*Q2_s - 8*I2G_s*Q2_s - 2*L1X_s^2*Q2_s*m1_s - 8*L1X_s^2*Q2_s*m2_s + 2*L2X_s^2*Q1_s*m2_s - 2*L2X_s^2*Q2_s*m2_s + 4*I1G_s*L2X_s*g_s*m2_s*cos(TH1_s + TH2_s) - 2*L1X_s*L2X_s^2*g_s*m2_s^2*cos(TH1_s) + 4*L1X_s*L2X_s*Q1_s*m2_s*cos(TH2_s) - 8*L1X_s*L2X_s*Q2_s*m2_s*cos(TH2_s) - 4*I2G_s*L1X_s*g_s*m1_s*cos(TH1_s) - 8*I2G_s*L1X_s*g_s*m2_s*cos(TH1_s) + L1X_s*L2X_s^3*TH1D_s^2*m2_s^2*sin(TH2_s) + 4*L1X_s^3*L2X_s*TH1D_s^2*m2_s^2*sin(TH2_s) + L1X_s*L2X_s^3*TH2D_s^2*m2_s^2*sin(TH2_s) + 4*L1X_s^2*L2X_s*g_s*m2_s^2*cos(TH1_s + TH2_s) + L1X_s^2*L2X_s*g_s*m1_s*m2_s*cos(TH1_s + TH2_s) + 4*I1G_s*L1X_s*L2X_s*TH1D_s^2*m2_s*sin(TH2_s) + 4*I2G_s*L1X_s*L2X_s*TH1D_s^2*m2_s*sin(TH2_s) + 4*I2G_s*L1X_s*L2X_s*TH2D_s^2*m2_s*sin(TH2_s) - L1X_s*L2X_s^2*g_s*m1_s*m2_s*cos(TH1_s) + 2*L1X_s*L2X_s^3*TH1D_s*TH2D_s*m2_s^2*sin(TH2_s) + L1X_s^3*L2X_s*TH1D_s^2*m1_s*m2_s*sin(TH2_s) + 2*L1X_s*L2X_s^2*g_s*m2_s^2*cos(TH1_s + TH2_s)*cos(TH2_s) + 4*L1X_s^2*L2X_s^2*TH1D_s^2*m2_s^2*cos(TH2_s)*sin(TH2_s) + 2*L1X_s^2*L2X_s^2*TH2D_s^2*m2_s^2*cos(TH2_s)*sin(TH2_s) - 4*L1X_s^2*L2X_s*g_s*m2_s^2*cos(TH1_s)*cos(TH2_s) - 2*L1X_s^2*L2X_s*g_s*m1_s*m2_s*cos(TH1_s)*cos(TH2_s) + 8*I2G_s*L1X_s*L2X_s*TH1D_s*TH2D_s*m2_s*sin(TH2_s) + 4*L1X_s^2*L2X_s^2*TH1D_s*TH2D_s*m2_s^2*cos(TH2_s)*sin(TH2_s)))/(16*I1G_s*I2G_s + 4*L1X_s^2*L2X_s^2*m2_s^2 + 4*I2G_s*L1X_s^2*m1_s + 4*I1G_s*L2X_s^2*m2_s + 16*I2G_s*L1X_s^2*m2_s + L1X_s^2*L2X_s^2*m1_s*m2_s - 4*L1X_s^2*L2X_s^2*m2_s^2*cos(TH2_s)^2);
R5 = subs(QQ1,[Q1_s,Q2_s,TH1_s,TH2_s,TH1D_s,TH2D_s,TH1DD_s,TH2DD_s,'m1_s','m2_s','L1X_s','L2X_s','I1G_s','I2G_s','g_s'],[u1,u2,TH1_s,TH1D_s,TH2_s,TH2D_s,TH1DD_s,TH2DD_s,m1,m2,L1x,L2x,I1g,I2g,g]);
R6 = subs(QQ2,[Q1_s,Q2_s,TH1_s,TH2_s,TH1D_s,TH2D_s,TH1DD_s,TH2DD_s,'m1_s','m2_s','L1X_s','L2X_s','I1G_s','I2G_s','g_s'],[u1,u2,TH1_s,TH1D_s,TH2_s,TH2D_s,TH1DD_s,TH2DD_s,m1,m2,L1x,L2x,I1g,I2g,g]);
O = [TH1_s;TH1D_s;TH2_s;TH2D_s];
P = [TH1D_s;R5;TH2D_s;R6];
O = subs(O,[TH1_s,TH1D_s,TH2_s,TH2D_s],[n5,n6,n8,n9]);
P = subs(P,[TH1_s,TH1D_s,TH2_s,TH2D_s,u1,u2],[n5,n6,n8,n9,u1,u2]);
A = jacobian(P,O);
Alin = subs(A, [n5,n6,n8,n9], [theta1_0,0,theta2_0,0]);
B = jacobian(P, [u1;u2]);
Blin = subs(B, [n5,n6,n8,n9], [theta1_0,0,theta2_0,0]);
C = [1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 0 1];
D = [0,0;0,0;0,0;0,0];
Q = C'*C;
R = diag([1, 1]);
A = double(Alin);
B = double(Blin);
C = [1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 0 1];
D = [0,0;0,0;0,0;0,0];
Q = C'*C;
R = diag([1, 1]);
[FD,K,E] = lqr(A,B,Q,R);


QQ11 = subs(EOM_TH1,[m1_s, m2_s, g_s, I1G_s, I2G_s, L1X_s, L2X_s, b1_s, b2_s, TH1_s, TH1D_s, TH1DD_s, TH2_s, TH2D_s, TH2DD_s],[m1,m2,g,I1g,I2g,L1x,L2x,b1_tlm,b2_tlm,theta1_0,theta1_dot_0,0,theta2_0,theta2_dot_0,0]);
QQ22 = subs(EOM_TH2,[m1_s, m2_s, g_s, I1G_s, I2G_s, L1X_s, L2X_s, b1_s, b2_s, TH1_s, TH1D_s, TH1DD_s, TH2_s, TH2D_s, TH2DD_s],[m1,m2,g,I1g,I2g,L1x,L2x,b1_tlm,b2_tlm,theta1_0,theta1_dot_0,0,theta2_0,theta2_dot_0,0]);
QQ11 = double(solve(QQ11,Q1_s));
QQ22 = double(solve(QQ22,Q2_s));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% RIADENIE
cas = 0.01;
T = 35;

disp('[DONE]')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% SIMULATION SECTION

disp('Simulating...');

% SIMULINK
if sim_model == 'model_a1'
    sim('model_a1');

    % CONVERT COLUMNS TO RAWS IN VARIABLES
    tout = tout.';
    
    th1 = TH1.Data(:,1).';
    th1d = TH1_D.Data(:,1).';
    th1dd = TH1_DD.Data(:,1).';
    
    th1sm = TH1SM.Data(:,1).';
    th1dsm = TH1_DSM.Data(:,1).';
    th1ddsm = TH1_DDSM.Data(:,1).';

    theta12sim1 = theta12Simulink.Data(:,1).';
    theta12sim2 = theta12Simulink.Data(:,2).';

    th2 = TH2.Data(:,1).';
    th2sm = TH2SM.Data(:,1).';

    % SAVE VARIABLES TO FILE FOR NEXT USAGE IN PYTHON APP
    save('output','tout','th1','th1d','th1dd','th1sm','th1dsm','th1ddsm','theta12sim1','theta12sim2','th2','th2sm');
    
elseif sim_model == 'model_a2'
    sim('model_a2');

    % CONVERT COLUMNS TO RAWS IN VARIABLES
    tout = tout.';

    th1poz1 = LQR_TH1_Poz.Data(:,1).';
    th1poz2 = LQR_TH1_Poz.Data(:,2).';
    
    th2poz1 = LQR_TH2_Poz.Data(:,1).';
    th2poz2 = LQR_TH2_Poz.Data(:,2).';
    
    th1poz11 = LQR_TH1_Poz1.Data(:,1).';
    th1poz12 = LQR_TH1_Poz1.Data(:,2).';
    
    th2poz11 = LQR_TH2_Poz1.Data(:,1).';
    th2poz12 = LQR_TH2_Poz1.Data(:,2).';

    % SAVE VARIABLES TO FILE FOR NEXT USAGE IN PYTHON APP
    save('output','tout','th1poz1','th1poz2','th2poz1','th2poz2','th1poz11','th1poz12','th2poz11','th2poz12');
    
elseif sim_model == 'model_a3'
    sim('model_a3');

    % CONVERT COLUMNS TO RAWS IN VARIABLES
    tout = tout.';

    th1pozsmc1 = TH1_Poz_SMC.Data(:,1).';
    th1pozsmc2 = TH1_Poz_SMC.Data(:,2).';
    
    th2pozsmc1 = TH2_Poz_SMC.Data(:,1).';
    th2pozsmc2 = TH2_Poz_SMC.Data(:,2).';
    
    th1poze = TH1_Poz_e.Data(:,1).'; 
    th2poze = TH2_Poz_e.Data(:,1).';

    % SAVE VARIABLES TO FILE FOR NEXT USAGE IN PYTHON APP
    save('output','tout','th1pozsmc1','th1pozsmc2','th2pozsmc1','th2pozsmc2','th1poze','th2poze');
    
else
    disp('Wrong model name');
end

disp('[DONE]')
