FROM registry.gitlab.com/cann1bunny/matpywebapp/matlab:latest

LABEL MAINTAINER=Cann1Bunny

# Install dependencies, matlab-engine, set rights for nginx
RUN sudo apt-get update && sudo apt-get install -y python3 python3-pip nginx && \
    cd /usr/local/MATLAB/extern/engines/python && \
    sudo python3 setup.py build --build-base=$(mktemp -d) install && \
    sudo usermod -a -G matlab www-data

# Add nginx config and application folder
ADD nginx.conf /etc/nginx/conf.d/nginx.conf
ADD matpywebapp /matpywebapp

# Set right for application folder and install application dependencies
RUN sudo chown -R matlab:matlab /matpywebapp && \
    cd /matpywebapp && \
    sudo -H pip3 install -r requirements.txt

WORKDIR /matpywebapp

# Open port for nginx
EXPOSE 80

# Start command
CMD sudo nginx -g "daemon on;" && uwsgi --ini app.ini