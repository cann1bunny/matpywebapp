# -*- coding: utf-8 -*-
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html

from app import app
from figures import figs

# Index page layout
layout_index = dbc.Container(className='index', children=[
    dbc.Row([
        dbc.Col([
            html.H1(className='index-1', children=[
                'Technická univerzita v Košiciach', html.Br(),
                'Fakulta elektrotechniky a informatiky', html.Br(),
                'Katedra kybernetiky a umelej inteligencie'
            ])
        ])
    ]),
    dbc.Row([
        dbc.Col([
            html.H1(className='index-2', children=[
                'Simulácia modelovania a riadenia modelov Dvojlinkového planárneho robota a robota manipulátora'
            ])
        ])
    ]),
    dbc.Row([
        dbc.Col([
            html.H2(className='index-3', children=[
                'Stránka je súčasťou diplomovej práce - Programová aplikácia pre simuláciu riadenia nelineárnych modelov robotických systémov s využitím efektívnych simulačných nástrojov', html.Br(), html.Br(), html.Br(),
                'Autor: Bc. Nadiia Bezerova', html.Br(),
                'Školiteľ: doc. Ing. Anna Jadlovská, PhD.', html.Br(),
                'Rok:  2020'
            ])
        ])
    ]),
    dbc.Row([
        dbc.Col([
            dbc.Button(
                className='index-button',
                size='lg',
                block=True,
                href='/apps',
                children='Start'
            )
        ])
    ])
])

# Applications page layout
layout_apps = dbc.Container(fluid=True, children=[
    dbc.Tabs([
        dbc.Tab(label='DVOJLINKOVÝ PLANÁRNY ROBOT', children=[
            dbc.Tabs([
                dbc.Tab(label='Modelovanie systému', children=[
                    dbc.Row(justify='center', children=[
                        dbc.Col(width=12, lg=4, children=[
                            html.H3('Simulácia modelu Dvojlinkového planárneho robota'),
                            dbc.Card([
                                dbc.CardBody([
                                    dbc.Row([
                                        dbc.Col(width=6, children=[
                                            dbc.Label('Dĺžka L1x [1-2]'),
                                            dbc.Input(
                                                id='l1',
                                                type='number',
                                                min=1.0,
                                                max=2.0,
                                                step=0.01
                                            ),
                                            dbc.Label('Dĺžka L2x [0.5-0.7]'),
                                            dbc.Input(
                                                id='l2',
                                                type='number',
                                                min=0.5,
                                                max=0.7,
                                                step=0.01
                                            )
                                        ]),
                                        dbc.Col(width=6, children=[
                                            dbc.Checklist(
                                                id='checklist_a1',
                                                options=[
                                                    {'label': 'HoldOn', 'value': 'holdon'}
                                                ],
                                                value=[]
                                            ),
                                            dbc.RadioItems(
                                                id='radioitems_a1',
                                                options=[
                                                    {'label': 'SIMULINK', 'value': 1},
                                                    {'label': 'SIMSCAPE MULTIBODY', 'value': 2},
                                                    {'label': 'POROVNANIE', 'value': 3}
                                                ],
                                                value=1
                                            ),
                                            dbc.Button(
                                                id='button_a1',
                                                children='SIMULÁCIA'
                                            )
                                        ])
                                    ])
                                ])
                            ]),
                            html.Div(className='img-div', children=[
                                html.Img(
                                    className='img-fluid img',
                                    src=app.get_asset_url('model_a1.png')
                                )
                            ])
                        ]),
                        dbc.Col(width=12, lg=4, children=[
                            dcc.Graph(
                                className='graph',
                                id='fig_a1_1',
                                figure=figs['a1_1'],
                                config={'displaylogo': False}
                            ),
                            dcc.Graph(
                                className='graph',
                                id='fig_a1_2',
                                figure=figs['a1_2'],
                                config={'displaylogo': False}
                            )
                        ]),
                        dbc.Col(width=12, lg=4, children=[
                            dcc.Graph(
                                className='graph',
                                id='fig_a1_3',
                                figure=figs['a1_3'],
                                config={'displaylogo': False}
                            ),
                            dcc.Graph(
                                className='graph',
                                id='fig_a1_4',
                                figure=figs['a1_4'],
                                config={'displaylogo': False}
                            )
                        ])
                    ])
                ]),
                dbc.Tab(label='Optimálne stavové riadenie LQ', children=[
                    dbc.Row(justify='center', children=[
                        dbc.Col(width=12, lg=6, children=[
                            html.H3('Kompenzácia poruchy pomocou spätnoväzobného zosilnenia Kx'),
                            dbc.Card([
                                dbc.CardBody([
                                    dbc.Row([
                                        dbc.Col(width=4, children=[
                                            dbc.Label('Dĺžka L1x [1-2]'),
                                            dbc.Input(
                                                id='dlzka1',
                                                type='number',
                                                min=1.0,
                                                max=2.0,
                                                step=0.01
                                            ),
                                            dbc.Label('Dĺžka L1x [0.5-0.7]'),
                                            dbc.Input(
                                                id='dlzka2',
                                                type='number',
                                                min=0.5,
                                                max=0.7,
                                                step=0.01
                                            )
                                        ]),
                                        dbc.Col(width=4, children=[
                                            dbc.Label('Pracovný bod th1'),
                                            dbc.Input(
                                                id='theta1',
                                                type='number'
                                            ),
                                            dbc.Label('Pracovný bod th2'),
                                            dbc.Input(
                                                id='theta2',
                                                type='number'
                                            )
                                        ]),
                                        dbc.Col(width=4, children=[
                                            dbc.Checklist(
                                                id='checklist_a2',
                                                options=[
                                                    {'label': 'HoldOn', 'value': 'holdon'}
                                                ],
                                                value=[]
                                            ),
                                            dbc.RadioItems(
                                                id='radioitems_a2',
                                                options=[
                                                    {'label': 'SIMULINK', 'value': 1},
                                                    {'label': 'SIMSCAPE MULTIBODY', 'value': 2},
                                                    {'label': 'POROVNANIE', 'value': 3}
                                                ],
                                                value=1
                                            ),
                                            dbc.Button(
                                                id='button_a2',
                                                children='SIMULÁCIA'
                                            )
                                        ])
                                    ])
                                ])
                            ]),
                            html.Div(className='img-div', children=[
                                html.Img(
                                    className='img-fluid img',
                                    src=app.get_asset_url('model_a2.png')
                                )
                            ])
                        ]),
                        dbc.Col(width=12, lg=6, children=[
                            dcc.Graph(
                                className='graph',
                                id='fig_a2_1',
                                figure=figs['a2_1'],
                                config={'displaylogo': False}
                            ),
                            dcc.Graph(
                                className='graph',
                                id='fig_a2_2',
                                figure=figs['a2_2'],
                                config={'displaylogo': False}
                            )
                        ])
                    ])
                ]),
                dbc.Tab(label='Riadenie v kĺzavom režime SMC', children=[
                    dbc.Row(justify='center', children=[
                        dbc.Col(width=12, lg=6, children=[
                            html.H3('Sledovanie požadovanej trajektorie'),
                            dbc.Card([
                                dbc.CardBody([
                                    dbc.Row([
                                        dbc.Col(width=8, children=[
                                            dbc.Label('Požadovaná poloha (rad)'),
                                            dbc.Input(
                                                id='trajektoria',
                                                type='text'
                                            )
                                        ]),
                                        dbc.Col(width=4, children=[
                                            dbc.Checklist(
                                                id='checklist_a3',
                                                options=[
                                                    {'label': 'HoldOn', 'value': 'holdon'}
                                                ],
                                                value=[]
                                            ),
                                            dbc.RadioItems(
                                                id='radioitems_a3',
                                                options=[
                                                    {'label': 'SIMULINK', 'value': 1},
                                                    {'label': 'SIMSCAPE MULTIBODY', 'value': 2},
                                                    {'label': 'POROVNANIE', 'value': 3}
                                                ],
                                                value=1
                                            ),
                                            dbc.Button(
                                                id='button_a3',
                                                children='SIMULÁCIA'
                                            )
                                        ])
                                    ])
                                ])
                            ]),
                            html.Div(className='img-div', children=[
                                html.Img(
                                    className='img-fluid img',
                                    src=app.get_asset_url('model_a3.png')
                                )
                            ])
                        ]),
                        dbc.Col(width=12, lg=6, children=[
                            dcc.Graph(
                                className='graph',
                                id='fig_a3_1',
                                figure=figs['a3_1'],
                                config={'displaylogo': False}
                            ),
                            dcc.Graph(
                                className='graph',
                                id='fig_a3_2',
                                figure=figs['a3_2'],
                                config={'displaylogo': False}
                            )
                        ])
                    ])

                ])
            ])
        ]),
        dbc.Tab(label='ROBOT MANIPULÁTOR', children=[
            dbc.Tabs([
                dbc.Tab(label='Modelovanie systému', children=[
                    dbc.Row([
                        dbc.Col(width=12, lg=6, children=[
                            html.H3('Simulácia modelu robota manipulátora'),
                            dbc.Card([
                                dbc.CardBody([
                                    dbc.Row([
                                        dbc.Col([
                                            dbc.Checklist(
                                                id='checklist_b1',
                                                options=[
                                                    {'label': 'HoldOn', 'value': 'holdon'}
                                                ],
                                                value=[]
                                            ),
                                            dbc.RadioItems(
                                                id='radioitems_b1',
                                                options=[
                                                    {'label': 'KLASICKÁ SCHÉMA SIMULINK', 'value': 1},
                                                    {'label': 'SIMULINK', 'value': 2},
                                                    {'label': 'SIMSCAPE', 'value': 3},
                                                    {'label': 'POROVNANIE', 'value': 4}
                                                ],
                                                value=1
                                            ),
                                            dbc.Button(
                                                id='button_b1',
                                                children='SIMULÁCIA'
                                            )
                                        ])
                                    ])
                                ])
                            ]),
                            html.Div(className='img-div', children=[
                                html.Img(
                                    className='img-fluid img',
                                    src=app.get_asset_url('model_b1.png')
                                )
                            ])
                        ]),
                        dbc.Col(width=12, lg=6, children=[
                            html.Div(className='img-div', children=[
                                html.Img(
                                    className='img-fluid formula',
                                    src=app.get_asset_url('formula_b1.png')
                                )
                            ]),
                            dcc.Graph(
                                className='graph',
                                id='fig_b1_1',
                                figure=figs['b1_1'],
                                config={'displaylogo': False}
                            ),
                            dcc.Graph(
                                className='graph',
                                id='fig_b1_2',
                                figure=figs['b1_2'],
                                config={'displaylogo': False}
                            )
                        ])
                    ])
                ]),
                dbc.Tab(label='Spätnoväzobné Optimálne Riadenie', children=[
                    dbc.Row([
                        dbc.Col(width=12, lg=6, children=[
                            html.H3('Sledovanie optimálnych trajektórií pohybu'),
                            dbc.Card([
                                dbc.CardBody([
                                    dbc.Row([
                                        dbc.Col(width=4, children=[
                                            dbc.Label('Požadovaná poloha th1'),
                                            dbc.Input(
                                                id='stav1',
                                                type='number'
                                            ),
                                            dbc.Label('Požadovaná poloha th2'),
                                            dbc.Input(
                                                id='stav2',
                                                type='number'
                                            )
                                        ]),
                                        dbc.Col(width=4, children=[
                                            dbc.Label('Časový krok'),
                                            dbc.Input(
                                                id='krok',
                                                type='number'
                                            ),
                                            dbc.Label('Požadovaná presnosť'),
                                            dbc.Input(
                                                id='presnost',
                                                type='number'
                                            )
                                        ]),
                                        dbc.Col(width=4, children=[
                                            dbc.Checklist(
                                                id='checklist_b2',
                                                options=[
                                                    {'label': 'HoldOn', 'value': 'holdon'}
                                                ],
                                                value=[]
                                            ),
                                            dbc.RadioItems(
                                                id='radioitems_b2',
                                                options=[
                                                    {'label': 'SIMULINK', 'value': 1},
                                                    {'label': 'SIMSCAPE', 'value': 2},
                                                    {'label': 'POROVNANIE', 'value': 3}
                                                ],
                                                value=1
                                            ),
                                            dbc.Button(
                                                id='button_b2',
                                                children='SIMULÁCIA'
                                            )
                                        ])
                                    ])
                                ])
                            ]),
                            html.Img(
                                className='img-fluid',
                                src=app.get_asset_url('model_b2_1.jpg')
                            ),
                            html.Img(
                                className='img-fluid',
                                src=app.get_asset_url('model_b2_2.png')
                            )
                        ]),
                        dbc.Col(width=12, lg=3, children=[
                            dcc.Graph(
                                className='graph',
                                id='fig_b2_1',
                                figure=figs['b2_1'],
                                config={'displaylogo': False}
                            ),
                            dcc.Graph(
                                className='graph',
                                id='fig_b2_2',
                                figure=figs['b2_2'],
                                config={'displaylogo': False}
                            ),
                            dcc.Graph(
                                className='graph',
                                id='fig_b2_3',
                                figure=figs['b2_3'],
                                config={'displaylogo': False}
                            )
                        ]),
                        dbc.Col(width=12, lg=3, children=[
                            dcc.Graph(
                                className='graph',
                                id='fig_b2_4',
                                figure=figs['b2_4'],
                                config={'displaylogo': False}
                            ),
                            dcc.Graph(
                                className='graph',
                                id='fig_b2_5',
                                figure=figs['b2_5'],
                                config={'displaylogo': False}
                            )
                        ])
                    ])
                ])
            ])
        ])
    ])
])
