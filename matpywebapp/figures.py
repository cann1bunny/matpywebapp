# -*- coding: utf-8 -*-
import plotly.graph_objs as go

# Initiate figure objects as dictionary
figs = {'a1_1': go.Figure(), 'a1_2': go.Figure(), 'a1_3': go.Figure(), 'a1_4': go.Figure(), 'a2_1': go.Figure(),
        'a2_2': go.Figure(), 'a3_1': go.Figure(), 'a3_2': go.Figure(), 'b1_1': go.Figure(), 'b1_2': go.Figure(),
        'b2_1': go.Figure(), 'b2_2': go.Figure(), 'b2_3': go.Figure(), 'b2_4': go.Figure(), 'b2_5': go.Figure(),
        'b2_6': go.Figure()}

# Set some part of properties for all figures
for i in figs:
    figs[i].layout.title.x = 0.5
    figs[i].layout.showlegend = True
    figs[i].layout.xaxis.title.text = 'Čas (t)'

figs['a1_1'].layout.title.text = 'Poloha, rýchlosť a zrýchlenie prvého ramena'
figs['a1_1'].layout.yaxis.title.text = 'Poloha th1/th1SM, rýchlosť th1_d, zrýchlenie th1_dd'

figs['a1_2'].layout.title.text = 'Poloha prvého ramena modelu'
figs['a1_2'].layout.yaxis.title.text = 'Poloha th1, th1SM (rad)'

figs['a1_3'].layout.title.text = 'Poloha prvého a druhého ramena. Simulink'
figs['a1_3'].layout.yaxis.title.text = 'Ploha th1,poloha th2 (rad)'

figs['a1_4'].layout.title.text = 'Poloha druhého ramena modelu'
figs['a1_4'].layout.yaxis.title.text = 'Poloha th2, th2SM (rad)'

figs['a2_1'].layout.title.text = 'LQR riadenie'
figs['a2_1'].layout.yaxis.title.text = 'Poloha th1 (rad)'

figs['a2_2'].layout.title.text = 'LQR riadenie'
figs['a2_2'].layout.yaxis.title.text = 'Poloha th2 (rad)'

figs['a3_1'].layout.title.text = 'Sliding Mode Control'
figs['a3_1'].layout.yaxis.title.text = 'Poloha th1 (rad)'

figs['a3_2'].layout.title.text = 'Sliding Mode Control'
figs['a3_2'].layout.yaxis.title.text = 'Odchýlka polohy e1(t), e2(t)'

figs['b1_1'].layout.title.text = 'Časový priebeh polohy'
figs['b1_1'].layout.yaxis.title.text = 'Poloha r (m)'

figs['b1_2'].layout.title.text = 'Časový priebeh uhla'
figs['b1_2'].layout.yaxis.title.text = 'Uhol fi (rad)'

figs['b2_1'].layout.title.text = 'POR - optimálne trajektórie'
figs['b2_1'].layout.yaxis.title.text = 'Poloha x1, rýchlosť x2, uhol x3, uhlová rýchlosť x4'

figs['b2_2'].layout.title.text = 'SOR - zmena napätia u1(t) u2(t)'
figs['b2_2'].layout.yaxis.title.text = 'Napätie u1, u1 (V)'

figs['b2_3'].layout.title.text = 'POR - čassové odchýlky'
figs['b2_3'].layout.yaxis.title.text = 'Odchýlka polohy e_x1, odchýlka uhla e_x3'

figs['b2_4'].layout.title.text = 'SOR - priebeh optimálnych trajektórií'
figs['b2_4'].layout.yaxis.title.text = 'Rýchlosť x2, uhlová rýchlosť x4'

figs['b2_5'].layout.title.text = 'SOR - časový priebeh pôsobiacich poruch'
figs['b2_5'].layout.yaxis.title.text = 'Porúcha v1, v2 (V)'
