function vystup = vstupy_nabeh(t)
    vystup = 45*pi/180+30*pi/180*(1-cos(0*t));
end