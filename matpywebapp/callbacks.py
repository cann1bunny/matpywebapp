# -*- coding: utf-8 -*-
import dash
import plotly.graph_objs as go
from dash.dependencies import Input, Output
import matlab.engine
import scipy.io as sio

from app import app
from figures import figs


# Common operations for all tabs
def update_figures(target_figs, button_n_clicks, checklist_value, input_variables, sim_model):
    # HoldOn checkbox. Reset button counter to 1 click and figures data
    if 'holdon' not in checklist_value:
        button_n_clicks = 1
        for x in target_figs:
            x.update(data=[], overwrite=True)

    # Print app elements values to console. Just for debug
    print('Checked parameters: ', checklist_value)
    print('Number of button clicks: ', button_n_clicks)

    # Start Matlab process
    eng = matlab.engine.start_matlab()

    # Initiate input variables in Matlab workspace
    for x in input_variables:
        eng.workspace[x] = float(input_variables[x])

    # Initiate variable with Simulink model filename in Matlab workspace
    eng.workspace['sim_model'] = sim_model

    # Return data
    return eng, button_n_clicks


# Button A1
@app.callback(
    [Output('fig_a1_1', 'figure'),
     Output('fig_a1_2', 'figure'),
     Output('fig_a1_3', 'figure'),
     Output('fig_a1_4', 'figure')],
    [Input('button_a1', 'n_clicks'),
     Input('checklist_a1', 'value'),
     Input('radioitems_a1', 'value'),
     Input('l1', 'value'),
     Input('l2', 'value')]
)
def update_figures_a1_1(button_n_clicks, checklist_value, radioitems_value, l1_value, l2_value):
    changed_id = [p['prop_id'] for p in dash.callback_context.triggered][0]
    if 'button' in changed_id:

        # Target figures list
        target_figs = [figs['a1_1'], figs['a1_2'], figs['a1_3'], figs['a1_4']]

        # Input variables list
        input_variables = {
            'L1x': l1_value,
            'L2x': l2_value,
        }

        # Target Simulink model filename
        sim_model = 'model_a1'

        # Call main update function
        eng, button_n_clicks = update_figures(target_figs, button_n_clicks, checklist_value, input_variables, sim_model)

        # Start Matlab script
        eng.input_a1(nargout=0)

        # Get output variables from generated .mat file
        mf = sio.loadmat('output.mat')
        tout = mf['tout']
        th1 = mf['th1']
        th1d = mf['th1d']
        th1dd = mf['th1dd']
        th1sm = mf['th1sm']
        th1dsm = mf['th1dsm']
        th1ddsm = mf['th1ddsm']
        theta12sim1 = mf['theta12sim1']
        theta12sim2 = mf['theta12sim2']
        th2 = mf['th2']
        th2sm = mf['th2sm']

        # Convert multilayer NumPy array to single layer
        tout = tout[0]
        th1 = th1[0]
        th1d = th1d[0]
        th1dd = th1dd[0]
        th1sm = th1sm[0]
        th1dsm = th1dsm[0]
        th1ddsm = th1ddsm[0]
        theta12sim1 = theta12sim1[0]
        theta12sim2 = theta12sim2[0]

        th2 = th2[0]
        th2sm = th2sm[0]

        # Drawing function for TH values
        def draw_th():
            figs['a1_1'].add_trace(go.Scatter(x=tout, y=th1, name='TH1'))
            figs['a1_1'].add_trace(go.Scatter(x=tout, y=th1d, name='TH1_D'))
            figs['a1_1'].add_trace(go.Scatter(x=tout, y=th1dd, name='TH1_DD'))
            figs['a1_3'].add_trace(go.Scatter(x=tout, y=theta12sim1, name='TH1'))
            figs['a1_3'].add_trace(go.Scatter(x=tout, y=theta12sim2, name='TH2'))
            figs['a1_2'].add_trace(go.Scatter(x=tout, y=th1, name='TH1'))
            figs['a1_4'].add_trace(go.Scatter(x=tout, y=th2, name='TH2'))

        # Drawing function for THSM values
        def draw_thsm():
            figs['a1_1'].add_trace(go.Scatter(x=tout, y=th1sm, name='TH1SM'))
            figs['a1_1'].add_trace(go.Scatter(x=tout, y=th1dsm, name='TH1SM_D'))
            figs['a1_1'].add_trace(go.Scatter(x=tout, y=th1ddsm, name='TH1SM_DD'))
            figs['a1_2'].add_trace(go.Scatter(x=tout, y=th1sm, name='TH1SM'))
            figs['a1_4'].add_trace(go.Scatter(x=tout, y=th2sm, name='TH2SM'))

        # Add traces to figures using selected option on UI
        if radioitems_value == 1:
            draw_th()

        elif radioitems_value == 2:
            draw_thsm()

        elif radioitems_value == 3:
            draw_th()
            draw_thsm()

        # End callback
        return figs['a1_1'], figs['a1_2'], figs['a1_3'], figs['a1_4']
    else:
        return figs['a1_1'], figs['a1_2'], figs['a1_3'], figs['a1_4']


# Button A2
@app.callback(
    [Output('fig_a2_1', 'figure'),
     Output('fig_a2_2', 'figure')],
    [Input('button_a2', 'n_clicks'),
     Input('checklist_a2', 'value'),
     Input('radioitems_a2', 'value'),
     Input('dlzka1', 'value'),
     Input('dlzka2', 'value'),
     Input('theta1', 'value'),
     Input('theta2', 'value')]
)
def update_figures_a2(button_n_clicks, checklist_value, radioitems_value, dlzka1_value, dlzka2_value, theta1_value,
                      theta2_value):
    changed_id = [p['prop_id'] for p in dash.callback_context.triggered][0]
    if 'button' in changed_id:

        # Target figures list
        target_figs = [figs['a2_1'], figs['a2_2']]

        # Input variables list
        input_variables = {
            'L1x': dlzka1_value,
            'L2x': dlzka2_value,
            'theta1_0': theta1_value,
            'theta2_0': theta2_value,
        }

        # Target Simulink model filename
        sim_model = 'model_a2'

        # Call main update function
        eng, button_n_clicks = update_figures(target_figs, button_n_clicks, checklist_value, input_variables, sim_model)

        # Start Matlab script
        eng.input_a2(nargout=0)

        # Get output variables from generated .mat file
        mf = sio.loadmat('output.mat')
        tout = mf['tout']
        th1poz1 = mf['th1poz1']
        th1poz2 = mf['th1poz2']
        th2poz1 = mf['th2poz1']
        th2poz2 = mf['th2poz2']
        th1poz11 = mf['th1poz11']
        th1poz12 = mf['th1poz12']
        th2poz11 = mf['th2poz11']
        th2poz12 = mf['th2poz12']

        # Convert multilayer NumPy array to single layer
        tout = tout[0]
        th1poz1 = th1poz1[0]
        th1poz2 = th1poz2[0]
        th2poz1 = th2poz1[0]
        th2poz2 = th2poz2[0]
        th1poz11 = th1poz11[0]
        th1poz12 = th1poz12[0]
        th2poz11 = th2poz11[0]
        th2poz12 = th2poz12[0]

        # Drawing function for TH values
        def draw_th(clicks):
            if clicks == 1:
                figs['a2_1'].add_trace(go.Scatter(x=tout, y=th1poz2, name='TH 1 Požadovaná'))
                figs['a2_2'].add_trace(go.Scatter(x=tout, y=th2poz2, name='TH 2 Požadovaná'))
            figs['a2_1'].add_trace(go.Scatter(x=tout, y=th1poz1, name='TH 1'))
            figs['a2_2'].add_trace(go.Scatter(x=tout, y=th2poz1, name='TH 2'))

        # Drawing function for TH1 values
        def draw_th1(clicks):
            if clicks == 1:
                figs['a2_1'].add_trace(go.Scatter(x=tout, y=th1poz12, name='TH1 1 Požadovaná'))
                figs['a2_2'].add_trace(go.Scatter(x=tout, y=th2poz12, name='TH1 2 Požadovaná'))
            figs['a2_1'].add_trace(go.Scatter(x=tout, y=th1poz11, name='TH1 1'))
            figs['a2_2'].add_trace(go.Scatter(x=tout, y=th2poz11, name='TH1 2'))

        # Add traces to figures using selected option on UI
        if radioitems_value == 1:
            draw_th(button_n_clicks)

        elif radioitems_value == 2:
            draw_th1(button_n_clicks)

        elif radioitems_value == 3:
            draw_th(button_n_clicks)
            draw_th1(button_n_clicks)

        # End callback
        return figs['a2_1'], figs['a2_2']
    else:
        return figs['a2_1'], figs['a2_2']


# Button A3
@app.callback(
    [Output('fig_a3_1', 'figure'),
     Output('fig_a3_2', 'figure')],
    [Input('button_a3', 'n_clicks'),
     Input('checklist_a3', 'value'),
     Input('radioitems_a3', 'value'),
     Input('trajektoria', 'value')]
)
def update_figures_a3(button_n_clicks, checklist_value, radioitems_value, trajektoria_value):
    changed_id = [p['prop_id'] for p in dash.callback_context.triggered][0]
    if 'button' in changed_id:

        # Target figures list
        target_figs = [figs['a3_1'], figs['a3_2']]

        # Input variables list
        input_variables = {
            'ppp': trajektoria_value
        }

        # Target Simulink model filename
        sim_model = 'model_a3'

        # Call main update function
        eng, button_n_clicks = update_figures(target_figs, button_n_clicks, checklist_value, input_variables, sim_model)

        # # Set variable specific only for A3 button
        # eng.workspace['trajektoria'] = str(trajektoria_value)

        # Start Matlab script
        eng.input_a3(nargout=0)

        # Get output variables from generated .mat file
        mf = sio.loadmat('output.mat')
        tout = mf['tout']
        th1pozsmc1 = mf['th1pozsmc1']
        th1pozsmc2 = mf['th1pozsmc2']
        th2pozsmc1 = mf['th2pozsmc1']
        th2pozsmc2 = mf['th2pozsmc2']
        th1poze = mf['th1poze']
        th2poze = mf['th2poze']

        # Convert multilayer NumPy array to single layer
        tout = tout[0]
        th1pozsmc1 = th1pozsmc1[0]
        th1pozsmc2 = th1pozsmc2[0]
        th2pozsmc1 = th2pozsmc1[0]
        th2pozsmc2 = th2pozsmc2[0]
        th1poze = th1poze[0]
        th2poze = th2poze[0]

        # Drawing function for TH_Poz_SMC and TH_Poz_evalues
        def draw_thpozsmc_thpoze():
            figs['a3_1'].add_trace(go.Scatter(x=tout, y=th1pozsmc1, name='TH1'))
            figs['a3_1'].add_trace(go.Scatter(x=tout, y=th1pozsmc2, name='TH1 Poz'))
            figs['a3_1'].add_trace(go.Scatter(x=tout, y=th2pozsmc1, name='TH2'))
            figs['a3_1'].add_trace(go.Scatter(x=tout, y=th2pozsmc2, name='TH2 Poz'))
            figs['a3_2'].add_trace(go.Scatter(x=tout, y=th1poze, name='e1'))
            figs['a3_2'].add_trace(go.Scatter(x=tout, y=th2poze, name='e2'))

        # Drawing function for TH_Poz_SMC and TH_Poz_evalues
        def draw_thpozsmc_thpoze_sim():
            figs['a3_1'].add_trace(go.Scatter(x=tout, y=th1pozsmc1, name='TH1 Simscape'))
            figs['a3_1'].add_trace(go.Scatter(x=tout, y=th1pozsmc2, name='TH1 Poz'))
            figs['a3_1'].add_trace(go.Scatter(x=tout, y=th2pozsmc1, name='TH2 Simscape'))
            figs['a3_1'].add_trace(go.Scatter(x=tout, y=th2pozsmc2, name='TH2 Poz'))
            figs['a3_2'].add_trace(go.Scatter(x=tout, y=th1poze, name='e1 Simscape'))
            figs['a3_2'].add_trace(go.Scatter(x=tout, y=th2poze, name='e2 Simscape'))

        # Add traces to figures using selected option on UI
        if radioitems_value == 1:
            draw_thpozsmc_thpoze()

        elif radioitems_value == 2:
            draw_thpozsmc_thpoze_sim()

        elif radioitems_value == 3:
            draw_thpozsmc_thpoze()
            draw_thpozsmc_thpoze_sim()

        # End callback
        return figs['a3_1'], figs['a3_2']
    else:
        return figs['a3_1'], figs['a3_2']


# Button B1
@app.callback(
    [Output('fig_b1_1', 'figure'),
     Output('fig_b1_2', 'figure')],
    [Input('button_b1', 'n_clicks'),
     Input('checklist_b1', 'value'),
     Input('radioitems_b1', 'value')]
)
def update_figures_b1(button_n_clicks, checklist_value, radioitems_value):
    changed_id = [p['prop_id'] for p in dash.callback_context.triggered][0]
    if 'button' in changed_id:

        # Target figures list
        target_figs = [figs['b1_1'], figs['b1_2']]

        # Input variables list
        input_variables = {}

        # Target Simulink model filename
        sim_model = 'model_b1'

        # Call main update function
        eng, button_n_clicks = update_figures(target_figs, button_n_clicks, checklist_value, input_variables, sim_model)

        # Start Matlab script
        eng.input_b1(nargout=0)

        # Get output variables from generated .mat file
        mf = sio.loadmat('output.mat')
        tout = mf['tout']
        rs = mf['rs']
        ras = mf['ras']
        fis = mf['fis']
        fias = mf['fias']

        # Convert multilayer NumPy array to single layer
        tout = tout[0]
        rs = rs[0]
        ras = ras[0]
        fis = fis[0]
        fias = fias[0]

        # Drawing function for rS and fiS values
        def draw_rs_fis():
            figs['b1_1'].add_trace(go.Scatter(x=tout, y=rs, name='Poloha r (m) Klasická schéma'))
            figs['b1_2'].add_trace(go.Scatter(x=tout, y=fis, name='Uhol fi (rad) Klasická schéma'))

        # Drawing function for rAS and fiAS values
        def draw_ras_fias():
            figs['b1_1'].add_trace(go.Scatter(x=tout, y=ras, name='Poloha r (m) Simscape'))
            figs['b1_2'].add_trace(go.Scatter(x=tout, y=fias, name='Uhol fi (rad) Simscape'))

        # Drawing function for rASM and fiAS values
        def draw_rasm_fias():
            figs['b1_1'].add_trace(go.Scatter(x=tout, y=rs, name='Poloha r (m) Simscape'))
            figs['b1_2'].add_trace(go.Scatter(x=tout, y=fias, name='Uhol fi (rad) Simscape'))

        # Add traces to figures using selected option on UI
        if radioitems_value == 1:
            draw_rs_fis()

        elif radioitems_value == 2:
            draw_rs_fis()

        elif radioitems_value == 3:
            draw_rasm_fias()

        elif radioitems_value == 4:
            draw_rs_fis()
            draw_ras_fias()
            draw_rasm_fias()

        # End callback
        return figs['b1_1'], figs['b1_2']
    else:
        return figs['b1_1'], figs['b1_2']


# Button B2
@app.callback(
    [Output('fig_b2_1', 'figure'),
     Output('fig_b2_2', 'figure'),
     Output('fig_b2_3', 'figure'),
     Output('fig_b2_4', 'figure'),
     Output('fig_b2_5', 'figure')],
    [Input('button_b2', 'n_clicks'),
     Input('checklist_b2', 'value'),
     Input('radioitems_b2', 'value'),
     Input('stav1', 'value'),
     Input('stav2', 'value'),
     Input('krok', 'value'),
     Input('presnost', 'value')]
)
def update_figures_b2(button_n_clicks, checklist_value, radioitems_value, stav1_value, stav2_value, krok_value,
                      presnost_value):
    changed_id = [p['prop_id'] for p in dash.callback_context.triggered][0]
    if 'button' in changed_id:

        # Target figures list
        target_figs = [figs['b2_1'], figs['b2_2'], figs['b2_3']]

        # Input variables list
        input_variables = {
            'x1T': stav1_value,
            'x3T': stav2_value,
            'eps': presnost_value,
        }

        # Target Simulink model filename
        sim_model = 'model_b2'

        # Call main update function
        eng, button_n_clicks = update_figures(target_figs, button_n_clicks, checklist_value, input_variables, sim_model)

        # Start Matlab script
        eng.start(nargout=0)
        eng.input_b2(nargout=0)

        # Get output variables from generated .mat file
        mf = sio.loadmat('output.mat')
        t = mf['t']
        y1 = mf['y_1']
        y2 = mf['y_2']
        y3 = mf['y_3']
        y4 = mf['y_4']
        y11 = mf['y1_1']
        y12 = mf['y1_2']
        y13 = mf['y1_3']
        y14 = mf['y1_4']
        y21 = mf['y2_1']
        y22 = mf['y2_2']
        y41 = mf['y4_1']
        y42 = mf['y4_2']
        y51 = mf['y5_1']
        y52 = mf['y5_2']
        porucha_1 = mf['porucha_1']
        porucha_2 = mf['porucha_2']
        sor_1 = mf['sor_1']
        sor_2 = mf['sor_2']
        sor_3 = mf['sor_3']
        sor_4 = mf['sor_4']

        # Convert multilayer NumPy array to single layer
        t = t[0]
        y1 = y1[0]
        y2 = y2[0]
        y3 = y3[0]
        y4 = y4[0]
        y11 = y11[0]
        y12 = y12[0]
        y13 = y13[0]
        y14 = y14[0]
        y21 = y21[0]
        y22 = y22[0]
        y41 = y41[0]
        y42 = y42[0]
        y51 = y51[0]
        y52 = y52[0]
        porucha_1 = porucha_1[0]
        porucha_2 = porucha_2[0]
        sor_1 = sor_1[0]
        sor_2 = sor_2[0]
        sor_3 = sor_3[0]
        sor_4 = sor_4[0]

        # Drawing function for y, y2 and y4 values
        def draw_y_y2_y4():
            figs['b2_1'].add_trace(go.Scatter(x=t, y=y11, name='x1'))
            figs['b2_1'].add_trace(go.Scatter(x=t, y=y2, name='x2'))
            figs['b2_1'].add_trace(go.Scatter(x=t, y=y13, name='x3'))
            figs['b2_1'].add_trace(go.Scatter(x=t, y=y14, name='x4'))
            figs['b2_2'].add_trace(go.Scatter(x=t, y=y21, name='u1'))
            figs['b2_2'].add_trace(go.Scatter(x=t, y=y22, name='u2'))
            figs['b2_3'].add_trace(go.Scatter(x=t, y=y51, name='e1'))
            figs['b2_3'].add_trace(go.Scatter(x=t, y=y52, name='e2'))

        # Drawing function for y1, y2 and y5 values
        def draw_y1_y2_y5():
            figs['b2_1'].add_trace(go.Scatter(x=t, y=y11, name='x1 Simscape'))
            figs['b2_1'].add_trace(go.Scatter(x=t, y=y12, name='x2 Simscape'))
            figs['b2_1'].add_trace(go.Scatter(x=t, y=y13, name='x3 Simscape'))
            figs['b2_1'].add_trace(go.Scatter(x=t, y=y14, name='x4 Simscape'))
            figs['b2_2'].add_trace(go.Scatter(x=t, y=y21, name='u1 Simscape'))
            figs['b2_2'].add_trace(go.Scatter(x=t, y=y22, name='u2 Simscape'))
            figs['b2_3'].add_trace(go.Scatter(x=t, y=y51, name='e1 Simscape'))
            figs['b2_3'].add_trace(go.Scatter(x=t, y=y52, name='e2 Simscape'))

        # Add traces to figures using selected option on UI
        if radioitems_value == 1:
            draw_y_y2_y4()

        elif radioitems_value == 2:
            draw_y1_y2_y5()

        elif radioitems_value == 3:
            draw_y_y2_y4()
            draw_y1_y2_y5()

        # Draw another figures. I don't know in what option they suppose to be
        figs['b2_4'].add_trace(go.Scatter(x=t, y=sor_1, name='x2*'))
        figs['b2_4'].add_trace(go.Scatter(x=t, y=sor_2, name='x2'))
        figs['b2_4'].add_trace(go.Scatter(x=t, y=sor_3, name='x4*'))
        figs['b2_4'].add_trace(go.Scatter(x=t, y=sor_4, name='x4'))
        figs['b2_5'].add_trace(go.Scatter(x=t, y=porucha_1, name='v1'))
        figs['b2_5'].add_trace(go.Scatter(x=t, y=porucha_2, name='v2'))

        # End callback
        return figs['b2_1'], figs['b2_2'], figs['b2_3'], figs['b2_4'], figs['b2_5']
    else:
        return figs['b2_1'], figs['b2_2'], figs['b2_3'], figs['b2_4'], figs['b2_5']
