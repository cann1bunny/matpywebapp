function K = sor()
load k
load x
load u
global X U K1 K2 I23 mb mr k Ifr mz mrr m1 m2 m3 m4 hustota

mz=35;
mrr=62.5;
m1=52;
m2=78; 
m3=90;
m4=125;
K1=281.4;
K2=291;
Ifr=82.5;
r0=0.250;
I23=Ifr+((m2+m3+m4)*r0^2);
I=I23;
mb=mz+m1;
mr=mrr+mb+m2;
hustota = 1000; % kg/m3

L1x     = 1.00; % m
L1y     = 0.050; % m
L1z     = 0.005; % m
mass1   = hustota * L1x*L1y*L1z; % kg

I1g      = mass1*(L1x^2 + L1y^2)/12; % inrcia J

I1o      = I1g + mass1*(L1x/2)^2;

L2x     = 0.5; % m
L2y     = 0.05; % m
L2z     = 0.005; % m
mass2   = hustota * L2x*L2y*L2z; % kg

I2g      = mass2*(L2x^2 + L2y^2)/12;

I2o      = I2g + mass2*(L2x/2)^2;


syms x1 x2 x3 x4 u1 u2
Dx=[x2;
        (mb/mr)*x1*(x4^2)+(K1/mr)*u1;
        x4;
        -((2*mb*x1*x2*x4)/(I23+mb*(x1^2)))+(K2*u2/(I23+mb*(x1^2)))];

    Q = eye(4);
    R = eye(2)*0.05;

    A0 = [  diff(Dx(1),'x1'), diff(Dx(1), 'x2'), diff(Dx(1), 'x3'), diff(Dx(1), 'x4');
            diff(Dx(2),'x1'), diff(Dx(2), 'x2'), diff(Dx(2), 'x3'), diff(Dx(2), 'x4');
            diff(Dx(3),'x1'), diff(Dx(3), 'x2'), diff(Dx(3), 'x3'), diff(Dx(3), 'x4');
            diff(Dx(4),'x1'), diff(Dx(4), 'x2'), diff(Dx(4), 'x3'), diff(Dx(4), 'x4')];
   
    B0 = [  diff(Dx(1), 'u1'), diff(Dx(1), 'u2');
            diff(Dx(2), 'u1'), diff(Dx(2), 'u1');
            diff(Dx(3), 'u1'), diff(Dx(3), 'u1');
            diff(Dx(4), 'u1'), diff(Dx(4), 'u1')];
    
    
    x1t = X(k,1); % prebratie hodnot x,u v i-tom bode
    x2t = X(k,2);
    x3t = X(k,3);
    x4t = X(k,4);
    
    u1t = U(k,1);
    u2t = U(k,2);
    
    Ai = A0;
    Ai= subs(Ai,x1,x1t);
    Ai= subs(Ai,x2,x2t);
    Ai = subs(Ai,x3,x3t); 
    Ai = subs(Ai,x4,x4t);
    Ai = subs(Ai,u1,u1t);
    Ai = subs(Ai,u2,u2t);
    Ai = vpa(Ai,3);
    Ai = eval(Ai);	
    
    Bi = B0;
    Bi= subs(Bi,x1,x1t);
    Bi= subs(Bi,x2,x2t);
    Bi = subs(Bi,x3,x3t); 
    Bi = subs(Bi,x4,x4t);
    Bi = subs(Bi,u1,u1t);
    Bi = subs(Bi,u2,u2t);
    Bi = vpa(Bi,3);
    Bi = eval(Bi);	
    [K,S,E]= lqr(Ai,Bi,Q,R);
 k = k+1;
 save k
  
