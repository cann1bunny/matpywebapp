function ups = sliding_mode_control(e, e_dot, Kd, lambda, os)
    s = lambda*e+e_dot;
        
    if (abs(s) > os)    
        sat_s = sign(s);
    else
        sat_s = s/os;
    end
    
    ups = -Kd * sat_s;
end