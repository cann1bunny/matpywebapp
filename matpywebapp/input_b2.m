syms x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14 x15 x16 x17 x18 x19 x20 x21 x22 x23 x24 x25 x26 x27 x28 x29 x30 x31 x32 x33 x34 x35 x36 x37 x38 x39 x40 
syms u1 u2 x(t) r dr ddr fi dfi ddfi z dz ddz K3 u3
syms p1 p2 p3 p4 g

global X U mz mrr m1 m2 m3 m4 K1 K2 Ifr r0 I mb mr k

hustota = 1000;
cas = 0.01;
m1 = 52; 
m2 = 78; 
m3 = 90; 
m4 = 125; 
m5 = 141; 
mz = 35; 
mrr = 62.5; 
l = 1; 
h = 0.5; 
% K1=281.4;
% K2=291;
K1=281.4;
K2=291;
Ifr=82.5;
r0=0.250;
I=Ifr+((m2+m3+m4)*r0^2)
mb=mz+m1;
me=mb;
mr=mrr+mb+m2;
k=101;
% PRE SIMSCAPE
L1x = 1; 
L1y = 0.050; 
L1z = 0.005;
L2x = 0.5; 
L2y = 0.05; 
L2z = 0.005; 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STATE SPACE

Dx=[x2;
   (mb/mr)*x1*x4.^2+(K1/mr)*u1;
    x4;
    (-(2*mb*x1*x2*x4)/(I+mb*x1.^2))+(K2/(I+mb*x1.^2))*u2]; 

Dlin = Dx;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% KRITERIUM OPTIMALNEHO RIADENIA
J= 0.5*((u1.^2)+(u2.^2));

% HAMILTONIAN
H = -J+p1*Dx(1)+p2*Dx(2)+p3*Dx(3)+p4*Dx(4);

% VYJADRENIE KONVEKTOROV STAVU

Dx(5) = -diff(H,x1)
Dx(6) = -diff(H,x2)
Dx(7) = -diff(H,x3)
Dx(8) = -diff(H,x4)

%%%%%%%%%%%%
% ROVNICA ZAKONU RIADENIA

du1 = diff(H,'u1')
du2 = diff(H,'u2')
u1 = solve(du1==0,u1)
u2 = solve(du2==0,u2)

% DOSADENIE DO STATE SPACE

Dx=subs(Dx,'u1', u1) 
Dx=subs(Dx,'u2', u2)

Dx=subs(Dx,[p1,p2,p3,p4],[x5,x6,x7,x8])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
eps
i=0;
[t,x] = RungeKutta(@rovnicaDx,[0; 1],[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]);

% STAVY
x1T;
x2T = 0;
x3T;
x4T = 0;
p1 = 0;
p2 = 0;
p3 = 0;
p4 = 0;

% TEST PRESNOSTI 

while(  abs(x(length(x),1)-x1T) > eps...
        || abs(x(length(x),2)-x2T) > eps...
        || abs(x(length(x),3)-x3T) > eps...
        || abs(x(length(x),4)-x4T) > eps )

        [t,x] = RungeKutta(@rovnicaDx,[0; 1],[0, 0, 0, 0, p1, p2, p3, p4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]);
        i = i+1;
        
        % KOREKCIA
         delta_pi = [x(length(x),1)-x1T; x(length(x),2)-x2T; x(length(x),3)-x3T; x(length(x),4)-x4T]; 
         Uii = [-x(length(x),9) -x(length(x),10) -x(length(x),11) -x(length(x),12); -x(length(x),13) -x(length(x),14) -x(length(x),15) -x(length(x),16);-x(length(x),17) -x(length(x),18) -x(length(x),19) -x(length(x),20);-x(length(x),21) -x(length(x),22) -x(length(x),23) -x(length(x),24);];
         pi = inv(Uii)*delta_pi;
   
         % VYPOCET NOVYCH PP
         p1=x(1,5)+pi(1);
         p2=x(1,6)+pi(2);
         p3=x(1,7)+pi(3);
         p4=x(1,8)+pi(4);
         x(length(x),1);
         x(length(x),2);
         x(length(x),3);
         x(length(x),4);
end
u1=(402*x(:,6))/325;
u2=(4656*x(:,8))/(1392*x(:,1).^2 + 1613);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VYTAHOVANIE PARAMETROV STAVOV PRE SOR
X = x(:,1:4);
U = [u1,u2(:,end)];
save('u.mat','U')
save('x.mat','X')

load t
load x X
load u U
[Ai,Bi] = linmodel;  
Q = eye(4);
R = eye(2)*0.05;
[K,S,E] = lqr(Ai,Bi,Q,R);

disp('[DONE]')

%% SIMULATION SECTION
disp('Simulating...');
sim('model_b2');

% CONVERT COLUMNS TO RAWS IN VARIABLES
tout = tout.';

t = t(:,1).';

y_1 = y(:,1).';
y_2 = y(:,2).';
y_3 = y(:,3).';
y_4 = y(:,4).';

y1_1 = y1(:,1).';
y1_2 = y1(:,2).';
y1_3 = y1(:,3).';
y1_4 = y1(:,4).';

y2_1 = y2(:,1).';
y2_2 = y2(:,2).';

y4_1 = y4(:,1).';
y4_2 = y4(:,2).';

y5_1 = y5(:,1).';
y5_2 = y5(:,2).';

porucha_1 = Porucha(:,1).';
porucha_2 = Porucha(:,2).';

sor_1 = SOR(:,1).';
sor_2 = SOR(:,2).';
sor_3 = SOR(:,3).';
sor_4 = SOR(:,4).';

% SAVE VARIABLES TO FILE FOR NEXT USAGE IN PYTHON APP
save('output','tout','t','y_1','y_2','y_3','y_4','y1_1','y1_2','y1_3','y1_4','y2_1','y2_2','y4_1','y4_2','y5_1','y5_2','porucha_1','porucha_2','sor_1','sor_2','sor_3','sor_4')

disp('[DONE]')
% END OF SIMULATION SECTION
%%

function [Ai,Bi] = linmodel
        global X U mz mrr m1 m2 m3 m4 K1 K2 Ifr r0 I mb mr k
        load u
        load x
        syms x1 x2 x3 x4 u1 u2

        % STATE SPACE
        Dxx=[x2;
             (mb/mr)*x1*(x4^2)+(K1/mr)*u1;
             x4;
             -((2*mb*x1*x2*x4)/(I+mb*(x1^2)))+(K2*u2/(I+mb*(x1^2)))];

            k=101
                 x1t = X(k,1); % X,Y V ITOM BODE
                 x2t = X(k,2);
                 x3t = X(k,3);
                 x4t = X(k,4);
    
                 u1t = U(k,1);
                 u2t = U(k,2);
                 
                % LINEARIZACIA V PRACOVNOM BODE
                  A0 = [  diff(Dxx(1),'x1'), diff(Dxx(1), 'x2'), diff(Dxx(1), 'x3'), diff(Dxx(1), 'x4');
                  diff(Dxx(2),'x1'), diff(Dxx(2), 'x2'), diff(Dxx(2), 'x3'), diff(Dxx(2), 'x4');
                  diff(Dxx(3),'x1'), diff(Dxx(3), 'x2'), diff(Dxx(3), 'x3'), diff(Dxx(3), 'x4');
                  diff(Dxx(4),'x1'), diff(Dxx(4), 'x2'), diff(Dxx(4), 'x3'), diff(Dxx(4), 'x4')];
    
                  Ai = A0;
                  Ai= subs(Ai,x1,x1t);
                  Ai= subs(Ai,x2,x2t);
                  Ai = subs(Ai,x3,x3t); 
                  Ai = subs(Ai,x4,x4t);
                  Ai = subs(Ai,u1,u1t);
                  Ai = subs(Ai,u2,u2t);
                  Ai = vpa(Ai,3);
                  Ai = eval(Ai);	
    

                 B0 = [diff(Dxx(1), 'u1'), diff(Dxx(1), 'u2');
                       diff(Dxx(2), 'u1'), diff(Dxx(2), 'u1');
                       diff(Dxx(3), 'u1'), diff(Dxx(3), 'u1');
                       diff(Dxx(4), 'u1'), diff(Dxx(4), 'u1')]
    
                  Bi = B0;
                  Bi= subs(Bi,x1,x1t);
                  Bi= subs(Bi,x2,x2t);
                  Bi = subs(Bi,x3,x3t); 
                  Bi = subs(Bi,x4,x4t);
                  Bi = subs(Bi,u1,u1t);
                  Bi = subs(Bi,u2,u2t);
                  Bi = vpa(Bi,3);
                  Bi = eval(Bi);	  
end
