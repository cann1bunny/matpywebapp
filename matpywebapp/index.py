# -*- coding: utf-8 -*-
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

from app import app, server
from layouts import layout_index, layout_apps
import callbacks

# Page structure
app.index_string = '''
<!DOCTYPE html>
<html>
    <head>
        {%metas%}
        <title>Nadiia Bezerova - Simulácia modelovania a riadenia modelov Dvojlinkového planárneho robota a robota manipulátora</title>
        {%favicon%}
        {%css%}
    </head>
    <body>
        {%app_entry%}
        <footer>
            {%config%}
            {%scripts%}
            {%renderer%}
        </footer>
    </body>
</html>
'''

# Index layout
app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content')
])


# Index callback
@app.callback(Output('page-content', 'children'), [Input('url', 'pathname')])
def display_page(pathname):
    if pathname == "/apps":
        return layout_apps
    else:
        return layout_index


# Run debug server
if __name__ == '__main__':
    app.run_server(debug=True)
